﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Logging;


namespace ArithmeticOperations
{
    [TestClass]
    public class NSubstituteTests
    {
        [TestMethod]
        public void checkAddition()
        {

            NSubstituteTests obj=new NSubstituteTests();
            Addition nsub = Substitute.For<Addition>();
            int actual=nsub.add(1, 2);
            int expected = 3;
            Assert.AreEqual(expected, actual);
        }
    }
}
