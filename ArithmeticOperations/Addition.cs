﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ArithmeticOperations
{
    [TestClass]
    public class Addition
    {
        [TestMethod]
        public int add(int a, int b)
        {
            return a + b;
        }
    }
}
